# Torrent Adder for Torrentz2 Via Deluge

This add-on adds a clickable link next to torrent results in the torrentz2 page, which on clicking adds the torrent + tracker info to Deluge

This add-on requires WebUI to be enabled and it also required WebAPI to be enabled (refer below).




# Enabling Web API
[https://github.com/idlesign/deluge-webapi/tree/master/dist](https://github.com/idlesign/deluge-webapi/tree/master/dist)

* Download the .egg file
* Go to Deluge -> Preferences -> Plugins -> Install Plugin -> Choose the egg file
* Tick the Enable checkbox next to WebAPI Plugin
