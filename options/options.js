/*
Store the currently selected settings using browser.storage.local.
*/
function storeSettings() {

  function getDelugeHost() {
    const delugeHostInput = document.querySelector("input[name=deluge-host]");
    var v;
    if (delugeHostInput.value) {
      v = delugeHostInput.value;
    } else {
      v = delugeHostInput.getAttribute('data-default');
    }
    return v;
  }

  function getDelugePort() {
    const delugePortInput = document.querySelector("input[name=deluge-port]");
    var v;
    if (delugePortInput.value) {
      v = delugePortInput.value;
    } else {
      v = delugePortInput.getAttribute('data-default');
    }
    return v;
  }

  function getDelugePort() {
    const delugePasswordInput = document.querySelector("input[name=deluge-password]");
    var v;
    if (delugePasswordInput.value) {
      v = delugePasswordInput.value;
    } else {
      v = delugePasswordInput.getAttribute('data-default');
    }
    return v;
  }



  const delugeHost = getDelugeHost();
  const delugePort = getDelugePort();
  const delugePassword = getDelugePassword();
  browser.storage.local.set({
    delugeHost,
    delugePort,
    delugePassword
  });
}

/*
Update the options UI with the settings values retrieved from storage,
or the default settings if the stored settings are empty.
*/
function updateUI(restoredSettings) {
  const delugeHostInput = document.querySelector("input[name=deluge-host]");
  delugeHostInput.value = restoredSettings.delugeHost;

  const delugePortInput = document.querySelector("input[name=deluge-port]");
  delugePortInput.value = restoredSettings.delugePort;

  const delugePasswordInput = document.querySelector("input[name=deluge-password]");
  delugePasswordInput.value = restoredSettings.delugePort;

}

function onError(e) {
  console.error(e);
}

/*
On opening the options page, fetch stored settings and update the UI with them.
*/
const gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings.then(updateUI, onError);

/*
On clicking the save button, save the currently selected settings.
*/
const saveButton = document.querySelector("#save-button");
saveButton.addEventListener("click", storeSettings);
