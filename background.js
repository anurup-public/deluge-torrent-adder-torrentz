/*
Default settings. If there is nothing in storage, use these values.
*/
var defaultSettings = {
  delugeHost: "localhost",
  delugePort: 8112,
  delugePassword: "deluge"
};

/*
Generic error logger.
*/
function onError(e) {
  console.error(e);
}

/*
On startup, check whether we have stored settings.
If we don't, then store the default settings.
*/
function checkStoredSettings(storedSettings) {
  if (!storedSettings.delugeHost || !storedSettings.delugePort || !storedSettings.delugePassword ) {
    browser.storage.local.set(defaultSettings);
  }
}

const gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings.then(checkStoredSettings, onError);


getRandomIntInclusive = function(min, max) {
       min = Math.ceil(min);
       max = Math.floor(max);
       return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
     }

function login(hash) {
  const gettingStoredSettings = browser.storage.local.get();
  gettingStoredSettings.then((storedSettings) => {
    var http = new XMLHttpRequest();
    var url = "http://" + storedSettings.delugeHost + ":" + storedSettings.delugePort + "/json";

    http.open("POST", url, true);

    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Accept", "application/json");

    http.onreadystatechange = function() {
      //Call a function when the state changes.
      if(http.readyState == 4 && http.status == 200) {
          // alert(http.responseText);
          console.log('OK')
          storedSettings.cookie = http.getResponseHeader('Set-Cookie');
          addHash(hash);
      } else {
          console.error('NOT OK')
          console.error(http.status, http.responseText);
      }
    }
    http.send( JSON.stringify({
        "id": 1,
        "method": "auth.login",
        "params": ["deluge"]
    }) );
  }, onError);
}

/*
Forget browsing data, according to the settings passed in as storedSettings
or, if this is empty, according to the default settings.
*/
function addHash(hash) {
  const gettingStoredSettings = browser.storage.local.get();
  gettingStoredSettings.then((storedSettings) => {
        var http = new XMLHttpRequest();
        var url = "http://" + storedSettings.delugeHost + ":" + storedSettings.delugePort + "/json";

        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/json");
        http.setRequestHeader("Accept", "application/json");

        http.withCredentials = true;

        http.onreadystatechange = function() {
          //Call a function when the state changes.
          if(http.readyState == 4 && http.status == 200) {
              console.log('OK');
              console.log(http.responseText);
          } else {
              console.error('NOT OK')
              console.error(http.status, http.responseText);
          }
        }
        http.send( JSON.stringify( {
          "method": "web.add_torrents",
          "params": [
                      [
                        {
                          "path":
                          "magnet:?xt=urn:btih:000d6d7d2ee9107a2de626c26b2977ecf96ec25a",
                          "options":{
                            "file_priorities": [],
                            "add_paused": false,
                            "compact_allocation": false,
                            "max_connections": -1,
                            "max_download_speed": -1,
                            "max_upload_slots": -1,
                            "max_upload_speed": -1,
                            "prioritize_first_last_pieces":false
                          }
                        }
                      ]
                    ],
            "id": getRandomIntInclusive(1, 1000)
        }));
    }, onError);
  }



  function notify() {
    let dataTypesString = Object.keys(dataTypes).join(", ");
    let sinceString = new Date(since).toLocaleString();
    browser.notifications.create({
      "type": "basic",
      "title": "Removed browsing data",
      "message": `Removed ${dataTypesString}\nsince ${sinceString}`
    });
  }


browser.runtime.onMessage.addListener((message) => {
    login(message.hash);
});
/*
On click, fetch stored settings and forget browsing data.
*/
browser.browserAction.onClicked.addListener(() => {
  const gettingStoredSettings = browser.storage.local.get();
  gettingStoredSettings.then(forget, onError);
});
